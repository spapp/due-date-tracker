<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   due-date-tracker
 * @since     2017.03.11.
 */

ini_set('display_errors', true);
ini_set('display_startup_errors', true);
error_reporting(E_ALL | E_STRICT | E_DEPRECATED);

define('APPLICATION_PATH', dirname(__DIR__));

require_once(APPLICATION_PATH . '/src/Foo/Tracker/Calculable.php');
require_once(APPLICATION_PATH . '/src/Foo/Tracker/DueDate.php');
require_once(APPLICATION_PATH . '/src/Foo/Tracker/DueDate/Time.php');
require_once(APPLICATION_PATH . '/src/Foo/Tracker/DueDate/Interval.php');
