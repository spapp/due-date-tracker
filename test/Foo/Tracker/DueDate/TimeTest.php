<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   due-date-tracker
 * @since     2017.03.11.
 */

namespace Foo\Tracker\DueDate;

use DateTime;
use Foo\Tracker\DueDate\Time as DueDateTime;
use PHPUnit_Framework_TestCase as TestCase;

class TimeTest extends TestCase {

    protected static $time;

    public static function setUpBeforeClass() {
        self::$time = new DueDateTime(9, 10, 11);
    }

    public function testGetHour() {
        $this->assertEquals(self::$time->getHour(), 9);
    }

    public function testGetMinute() {
        $this->assertEquals(self::$time->getMinute(), 10);
    }

    public function testGetSeconds() {
        $this->assertEquals(self::$time->getSeconds(), 11);
    }

    public function test__toString() {
        $this->assertEquals(strval(self::$time), '09:10:11');
    }

    public function testGetDateTime() {
        $time = self::$time->getDateTime();

        $this->assertEquals($time->format('H:i:s'), '09:10:11');
    }
}
