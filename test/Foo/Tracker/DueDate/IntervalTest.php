<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   DueDateTracker
 * @since     2017. 03. 11.
 */

namespace Foo\Tracker\DueDate;

use Foo\Tracker\DueDate\Interval as DueDateInterval;
use PHPUnit_Framework_TestCase as TestCase;

class IntervalTest extends TestCase {

    public function testGetAsSeconds() {
        $seconds  = 5 + (30 * 60) + (8 * 60 * 60);
        $interval = new DueDateInterval(8, 30, 5);

        $this->assertEquals($interval->getAsSeconds(), $seconds);
    }
}
