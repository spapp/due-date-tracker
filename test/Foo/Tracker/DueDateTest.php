<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   due-date-tracker
 * @since     2017.03.11.
 */

namespace Foo\Tracker;

use DateTime;
use DateTimeZone;
use Foo\Tracker\DueDate\Interval as DueDateInterval;
use Foo\Tracker\DueDate\Time as DueDateTime;
use PHPUnit_Framework_TestCase as TestCase;

class DueDateTest extends TestCase {

    protected static $dueDate;
    protected static $dateTimeZone = 'Europe/Budapest';
    protected static $today        = '2017-03-11 23:41:00';

    public static function setUpBeforeClass() {
        self::$dueDate = new DueDate(new DateTime(self::$today, new DateTimeZone(self::$dateTimeZone)));
        self::$dueDate->setStartTime(new DueDateTime(9, 0, 0));
    }

    private static function getCalculation($reportTime, $turnaroundTime) {
        self::$dueDate->setTurnaroundTime($turnaroundTime);
        self::$dueDate->setReportTime(new DateTime($reportTime, new DateTimeZone(self::$dateTimeZone)));

        return self::$dueDate->calculate()->format('Y-m-d H:i:s');
    }

    public function test__construct() {
        $this->assertEquals(true, self::$dueDate instanceof Calculable);
    }

    public function testCalculate() {
        $tests = [
            // report time, turnaround time, expected value
            ['2017-03-11 07:00:00', new DueDateInterval(2, 0, 0), '2017-03-13 11:00:00'],
            ['2017-03-11 07:00:00', new DueDateInterval(2, 30, 0), '2017-03-13 11:30:00'],
            ['2017-03-10 16:00:00', new DueDateInterval(2, 30, 0), '2017-03-13 10:30:00'],
            ['2017-03-10 16:00:00', new DueDateInterval(12, 0, 0), '2017-03-14 12:00:00'],
            ['2017-03-08 06:00:00', new DueDateInterval(2, 0, 0), '2017-03-08 11:00:00'],
            ['2017-03-08 06:00:00', new DueDateInterval(40, 0, 0), '2017-03-14 17:00:00'],
            ['2017-03-08 06:00:00', new DueDateInterval(40, 15, 0), '2017-03-15 09:15:00']
        ];

        foreach ($tests as &$test) {
            $this->assertEquals($test[2], self::getCalculation($test[0], $test[1]));
        }
    }

    public function testCalculateSpecialDays() {
        $nonWorkingDay = new DateTime('2017-03-15', new DateTimeZone(self::$dateTimeZone));
        $workingDay    = new DateTime('2017-03-12', new DateTimeZone(self::$dateTimeZone));

        $tests = [
            // report time, turnaround time, expected value
            ['2017-03-11 07:00:00', new DueDateInterval(2, 0, 0), '2017-03-12 11:00:00'],
            ['2017-03-11 07:00:00', new DueDateInterval(2, 30, 0), '2017-03-12 11:30:00'],
            ['2017-03-10 16:00:00', new DueDateInterval(2, 30, 0), '2017-03-12 10:30:00'],
            ['2017-03-10 16:00:00', new DueDateInterval(12, 0, 0), '2017-03-13 12:00:00'],
            ['2017-03-08 06:00:00', new DueDateInterval(40, 0, 0), '2017-03-13 17:00:00'],
            ['2017-03-08 06:00:00', new DueDateInterval(40, 15, 0), '2017-03-14 09:15:00'],
            ['2017-03-08 06:00:00', new DueDateInterval(48, 15, 0), '2017-03-16 09:15:00']
        ];

        self::$dueDate->addSpecialWorkingDay($nonWorkingDay, DueDate::TYPE_SPECIAL_NONWORKING_DAYS);
        self::$dueDate->addSpecialWorkingDay($workingDay, DueDate::TYPE_SPECIAL_WORKING_DAYS);

        foreach ($tests as &$test) {
            $this->assertEquals($test[2], self::getCalculation($test[0], $test[1]));
        }
    }

}
