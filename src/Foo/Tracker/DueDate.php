<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   due-date-tracker
 * @since     2017.03.11.
 */

namespace Foo\Tracker;

use DateInterval;
use DateTime;
use DateTimeZone;
use Foo\Tracker\DueDate\Interval;
use Foo\Tracker\DueDate\Time;

/**
 * Class DueDate
 *
 * TODO handle timezone
 * TODO handle days-border shift
 *
 * @package Foo\Tracker
 */
class DueDate implements Calculable {

    const TYPE_SPECIAL_NONWORKING_DAYS = 1;
    const TYPE_SPECIAL_WORKING_DAYS    = 2;

    /**
     * @var DateTime
     */
    protected $startTime;

    /**
     * @var DateTime
     */
    protected $endTime;

    /**
     * @var int
     */
    protected $workingHours = 8; // 8 hour

    /**
     * ISO-8601 numeric representation of the day of the week; 1-based
     *
     * @var int[]
     */
    protected $workingDays = [1, 2, 3, 4, 5];

    /**
     * @var string[]
     */
    protected $specialNonworkingDays = [];

    /**
     * @var string[]
     */
    protected $specialWorkingDays = [];

    /**
     * @var Interval
     */
    protected $turnaroundTime;

    /**
     * @var DateTime
     */
    protected $reportTime;

    /**
     * @var DateTime
     */
    protected $today;

    /**
     * DueDate constructor.
     *
     * @param \DateTime $today
     */
    public function __construct(DateTime $today) {
        $this->today = $today;
    }

    /**
     * Sets the start of the working time
     *
     * @param \Foo\Tracker\DueDate\Time $time
     *
     * @return $this
     */
    public function setStartTime(Time $time) {
        $this->startTime = $time->toDateTime();

        $this->calculateEndTime();

        return $this;
    }

    /**
     * Sets the the working hours
     *
     * @param $workingHours
     *
     * @return $this
     */
    public function setWorkingHours($workingHours) {
        $this->workingHours = intval($workingHours); // TODO validation needs

        $this->calculateEndTime();

        return $this;
    }

    /**
     * Sets the work days
     *
     * @param int[] $workingDays
     *
     * @return $this
     */
    public function setWorkingDays(array $workingDays) {
        $this->workingDays = $workingDays;

        return $this;
    }

    /**
     * Sets turnaround time
     *
     * @param \Foo\Tracker\DueDate\Interval $interval
     *
     * @return $this
     */
    public function setTurnaroundTime(Interval $interval) {
        $this->turnaroundTime = $interval;

        return $this;
    }

    /**
     * Sets report time
     *
     * @param \DateTime $reportTime
     *
     * @return $this
     */
    public function setReportTime(DateTime $reportTime) {
        $this->reportTime = $reportTime;

        return $this;
    }

    /**
     * Adds special working or not working days
     *
     * @param \DateTime $specialWorkingDay
     * @param  int      $type
     *
     * @return $this
     */
    public function addSpecialWorkingDay(DateTime $specialWorkingDay, $type) {
        $date = $specialWorkingDay->format('Y-m-d');

        switch ($type) {
            case self::TYPE_SPECIAL_NONWORKING_DAYS:
                array_push($this->specialNonworkingDays, $date);
                break;
            case self::TYPE_SPECIAL_WORKING_DAYS:
                array_push($this->specialWorkingDays, $date);
                break;
            default:
                // TODO
        }

        return $this;
    }

    /**
     * Returns due date
     *
     * @return \DateTime|null
     */
    public function calculate() {
        $dueDate = $this->calculateStartDueDate(clone($this->reportTime));

        if (isset($dueDate)) {
            $turnaroundSeconds = $this->turnaroundTime->getAsSeconds();
            $endTime           = $dueDate->format('Y-m-d ') . $this->endTime->format('H:i:s');
            $endTime           = new DateTime($endTime, $this->endTime->getTimezone());
            $leftInShift       = $endTime->getTimestamp() - $dueDate->getTimestamp();

            if ($leftInShift >= $turnaroundSeconds) {
                $dueDate->modify('+' . $turnaroundSeconds . ' seconds');
            } else {
                $turnaroundSeconds -= $leftInShift;
                $dueDate           = $this->calculateDueDate($dueDate, $turnaroundSeconds);
            }

            return $dueDate;
        }

        return null;
    }

    /**
     * Returns due date
     * Helper method
     *
     * @param \DateTime $dueDate
     * @param  int      $turnaroundSeconds
     *
     * @return \DateTime|string
     */
    private function calculateDueDate(DateTime $dueDate, $turnaroundSeconds) {
        $workingSecounds = $this->workingHours * 60 * 60;

        do {
            $dueDate->modify('+1 days');

            $dueDate = $dueDate->format('Y-m-d ') . $this->startTime->format('H:i:s');
            $dueDate = new DateTime($dueDate, $this->startTime->getTimezone());

            if ($this->isAvailableDay($dueDate)) {
                if ($turnaroundSeconds <= $workingSecounds) {
                    $dueDate->modify('+' . $turnaroundSeconds . ' seconds');
                    $turnaroundSeconds = 0;
                } else {
                    $turnaroundSeconds -= $workingSecounds;
                }
            }
        } while ($turnaroundSeconds > 0);

        return $dueDate;
    }

    /**
     * Returns start time of the work process
     * Helper method
     *
     * @param DateTime $reportTime
     *
     * @return \DateTime|null
     */
    private function calculateStartDueDate(DateTime $reportTime) {
        if ($this->isAvailableDay($reportTime) && $this->isInTheShift($reportTime)) {
            return $reportTime;
        } else {
            for ($i = 0; $i < 7; $i++) {
                if ($this->isAvailableDay($reportTime)) {
                    $dueDate = $reportTime->format('Y-m-d ') . $this->startTime->format('H:i:s');

                    return new DateTime($dueDate, $this->startTime->getTimezone());
                } else {
                    $reportTime->modify('+1 day');
                }
            }
        }

        return null;
    }

    /**
     * Returns TRUE if the `$dateTime` is in the shift
     *
     * @param \DateTime $dateTime
     *
     * @return bool
     */
    private function isInTheShift(DateTime $dateTime) {
        $startTime = $this->startTime->format('His');
        $endTime   = $this->endTime->format('His');
        $date      = $dateTime->format('His');

        return $date >= $startTime && $date <= $endTime;
    }

    /**
     * Returns TRUE if the `$dateTime` is a workday
     *
     * @param \DateTime $dateTime
     *
     * @return bool
     */
    private function isAvailableDay(DateTime $dateTime) {
        $dayOfWheWeek = $dateTime->format('N');
        $date         = $dateTime->format('Y-m-d');

        return (
                in_array($dayOfWheWeek, $this->workingDays) || in_array($date, $this->specialWorkingDays)
            ) && !in_array($date, $this->specialNonworkingDays);
    }

    /**
     * Calculates the end of the working time
     */
    private function calculateEndTime() {
        if ($this->startTime instanceof DateTime) {
            $intervalSpec  = 'PT' . $this->workingHours . 'H'; // TODO handle float
            $this->endTime = clone($this->startTime);

            $this->endTime->add(new DateInterval($intervalSpec));
        }
    }
}
