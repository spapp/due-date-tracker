<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   due-date-tracker
 * @since     2017.03.11.
 */

namespace Foo\Tracker;

/**
 * Interface Calculable
 *
 * @package Foo\Tracker
 */
interface Calculable {

    /**
     * @return mixed
     */
    public function calculate();
}
