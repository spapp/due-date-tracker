<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   DueDateTracker
 * @since     2017. 03. 11.
 */

namespace Foo\Tracker\DueDate;

/**
 * Class Interval
 *
 * @package Foo\Tracker\DueDate
 */
class Interval {

    /**
     * @var int
     */
    private $hour;

    /**
     * @var int
     */
    private $minute;

    /**
     * @var int
     */
    private $seconds;

    /**
     * Interval constructor.
     *
     * @param int $hour
     * @param int $minute
     * @param int $seconds
     */
    public function __construct($hour, $minute, $seconds) {
        // TODO parameter validation needs
        $this->hour    = $hour;
        $this->minute  = $minute;
        $this->seconds = $seconds;
    }

    /**
     * Returns intervall as seconds
     *
     * @return int
     */
    public function getAsSeconds() {
        return $this->seconds + ($this->minute * 60) + ($this->hour * 3600);;
    }
}
