<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   due-date-tracker
 * @since     2017.03.11.
 */

namespace Foo\Tracker\DueDate;

use DateTime;
use DateTimeZone;

/**
 * Class Time
 *
 * Simple value object which contains a time.
 *
 * @package Foo\Tracker\DueDate
 */
class Time {

    const DEFULT_TIME_ZONE = 'Europe/Budapest';
    const FORMAT_TIME      = 'H:i:s';

    /**
     * @var DateTime
     * The time container
     */
    private $time;

    /**
     * Time constructor.
     *
     * @param int $hour
     * @param int $minute
     * @param int $seconds
     */
    public function __construct($hour, $minute = 0, $seconds = 0) {
        // TODO parameter validation needs
        $this->time = new DateTime(
            implode(':', [$hour, $minute, $seconds]),
            $this->getDateTimeZone()
        );
    }

    /**
     * Returns houre value
     *
     * @return int
     */
    public function getHour() {
        return intval($this->time->format('G'));
    }

    /**
     * Returns minute value
     *
     * @return int
     */
    public function getMinute() {
        return intval($this->time->format('i'));
    }

    /**
     * Returns seconds value
     *
     * @return int
     */
    public function getSeconds() {
        return intval($this->time->format('s'));
    }

    /**
     * Returns as an DateTime class
     *
     * @return DateTime
     */
    public function getDateTime() {
        return $this->time;
    }

    /**
     * @return \DateTime
     */
    public function toDateTime() {
        return $this->time;
    }

    /**
     * Magice method
     *
     * @return string
     */
    public function __toString() {
        return strval($this->time->format(self::FORMAT_TIME));
    }

    /**
     * Helper method for the constructor
     *
     * @return \DateTimeZone
     */
    private function getDateTimeZone() {
        $dateTimeZone = ini_get('date.timezone');

        if (!$dateTimeZone) {
            $dateTimeZone = self::DEFULT_TIME_ZONE;
        }

        return new DateTimeZone($dateTimeZone);
    }
}
