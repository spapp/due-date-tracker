default: test

doc:
	phpdoc -c phpdoc.xml

test:
	cd test;phpunit -c phpunit.xml

.PHONY: default doc test
